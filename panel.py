import bpy

class PT_Panel(bpy.types.Panel):
    bl_idname = "Image_Project_Panel"
    bl_label = "Image Projection"
    bl_category = "Camera Image Project"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"

    def draw(self, context):
        layout = self.layout

        row = layout.row()
        row.operator('view3d.proj_image', text ="Project Image")