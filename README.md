# Changelog

## 0.0.2

Version 0.0.2 is more robust when changing to a different camera with a different projection image.  Also removes a bug which prevented projection when the camera data name was mismatched to the object name.
Renamed project to Camera Image _Project_ to reflect URL.

## 0.0.1

Original Release.

# Camera Image Project

This is a Blender addon inteded to allow for a quick and easy way to dynamically project images onto geometry.

## Getting Started

### Prerequisites

Blender:
https://www.blender.org/download/

### Installing

Download the addon as a zip file

![alt text](img/Screenshot_1.png)

Go to Edit > Preferences, select the addons tab and choose 'install'

![alt text](img/Screenshot_2.png)

![alt text](img/Screenshot_3.png)

![alt text](img/Screenshot_4.png)

Select the downloaded zip file and install it

![alt text](img/Screenshot_5.png)

![alt text](img/Screenshot_6.png)

You should see the addon appear in a list in the preferences window

Be sure to enable it by selecting the checkbox next to its name

![alt text](img/Screenshot_7.png)

### Using

To find the addon make sure the cursor is in the 3D view and press 'n' to toggle the properties panel. It should appear on the right hand side

![alt text](img/Screenshot_9.png)

The addon should be under the tab 'Nick's Tools'.  Click on this tab to see the addon

![alt text](img/Screenshot_10.png)

In order to use the addon you must have a camera and some geometry

![alt text](img/Screenshot_11.png)

The camera must also contain a 'background' image which will be the image that is projected

In order to add a background image select the camera then go to the 'camera' tab of the camera properties and scroll down to 'background image'

![alt text](img/Screenshot_12.png)

![alt text](img/Screenshot_13.png)

Select an image from your computer

![alt text](img/Screenshot_14.png)

To see the results of the projection you will need to change the viewport mode to textured

![alt text](img/Screenshot_15.png)

In order to project the image onto the geometry you must select both the camera and the geometry and then press the 'Project Image!!!' button on the addon

![alt text](img/Screenshot_16.png)

![alt text](img/Screenshot_17.png)

You can also assign a shortcut to this button by right-clicking on it and choosing 'assign shortcut'

![alt text](img/Screenshot_19.png)

Observe the projected image

![alt text](img/Screenshot_18.png)