import bpy
import datetime

class OT_Operator(bpy.types.Operator):
    bl_idname = "view3d.proj_image"
    bl_label = "Simple operator"
    bl_description = "Project an image onto some geometry."

    def execute(self, context):
        
        # identify selected objects
        selobjs = bpy.context.selected_objects
        # identifty camera
        for ob in selobjs:
            if ob.type == "CAMERA":
                cam = ob
                break
        #get camera name
        camname = cam.name
        #identify image to be projected
        camimgs = bpy.data.objects['{}'.format(camname)].data.background_images
        camimg1 = camimgs[0].image.name
        #get image resolution
        camimg1_xres = camimgs[0].image.size[0]
        camimg1_yres = camimgs[0].image.size[1]
        # identify geometry 
        for ob in selobjs:
            if ob.type == "MESH":
                geom = ob
                break
        geomname = geom.name
        # deselect objects
        bpy.ops.object.select_all(action='TOGGLE')
        # select the geometry object
        geom.select_set(state=True)
        
        # Get material
        mat = bpy.data.materials.get("Material")
        nodes = mat.node_tree.nodes
        if mat is None:
            # create material
            mat = bpy.data.materials.new(name="Material")
        # Assign it to object
        if geom.data.materials:
            # assign to 1st material slot
            geom.data.materials[0] = mat
        else:
            # no slots
            geom.data.materials.append(mat)
            
        # activate geometry
        bpy.context.view_layer.objects.active = geom
        # put geometry into edit mode
        bpy.ops.object.editmode_toggle()
        # uv unwrap with smart unwrap
        bpy.ops.uv.smart_project()
        # go back into object mode
        bpy.ops.object.editmode_toggle()
        # abstract the modifier
        
        #create modifier
        bpy.ops.object.modifier_add(type='UV_PROJECT')
        mod = bpy.data.objects['{}'.format(geomname)].modifiers["UVProject"]
        # set uv project uvmap
        mod.uv_layer = "UVMap"
        # change the projector to the camera
        mod.projectors[0].object = cam
        #set uvproject aspect ratio
        mod.aspect_x = camimg1_xres
        mod.aspect_y = camimg1_yres
        
        #EDIT THE NODES
        #delete existing
        for node in mat.node_tree.nodes:
            mat.node_tree.nodes.remove(node)

        #identify nodetree
        ng = mat.node_tree
        
        # addd uv map node
        new_node = ng.nodes.new(type="ShaderNodeUVMap")
        new_node.location = (0, 0)
        uvnode = ng.nodes["UV Map"]
        uvnode.uv_map = "UVMap"
        
        #Add the image texture node  
        new_node = ng.nodes.new(type="ShaderNodeTexImage")
        new_node.location = (200, 0)
        imtex = ng.nodes["Image Texture"]
        #select the image from blender data
        imtex.image = bpy.data.images['{}'.format(camimg1)]
        #set the extension mode to clip
        imtex.extension = 'CLIP'
        
        #add the diffuse shader
        new_node = ng.nodes.new(type="ShaderNodeBsdfDiffuse")
        new_node.location = (500, 0)
        texdiffuse = ng.nodes["Diffuse BSDF"]    
        
        #make BG diffuse material
        new_node = ng.nodes.new(type="ShaderNodeBsdfDiffuse")
        new_node.location = (500, 200)
        bgdiffuse = ng.nodes["Diffuse BSDF.001"]

        #make mix node
        new_node = ng.nodes.new(type="ShaderNodeMixShader")
        new_node.location = (700, 100)
        mixnode = ng.nodes["Mix Shader"]
        
        #create shader output
        new_node = ng.nodes.new(type="ShaderNodeOutputMaterial")
        new_node.location = (900, 50)
        shadernode = ng.nodes["Material Output"]
        
        #connect nodes
        ng.links.new(texdiffuse.outputs[0], mixnode.inputs[2])
        ng.links.new(imtex.outputs[1], mixnode.inputs[0])
        ng.links.new(uvnode.outputs[0], imtex.inputs[0])
        ng.links.new(imtex.outputs[0], texdiffuse.inputs[0])
        ng.links.new(bgdiffuse.outputs[0], mixnode.inputs[1])
        ng.links.new(bgdiffuse.outputs[0], mixnode.inputs[1])
        ng.links.new(mixnode.outputs[0], shadernode.inputs[0])
            
        # reselect object and camera
        cam.select_set(state=True)
        geom.select_set(state=True)

        print (datetime.datetime.now())
        for nodes in ng.nodes:
            print(nodes.name)
        
        return {'FINISHED'}
