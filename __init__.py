# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

bl_info = {
    "name" : "Camera Image Projection",
    "author" : "Nick Masterton",
    "description" : "Addon to project an image onto an object with a single click.",
    "blender" : (2, 80, 0),
    "version" : (0, 0, 2),
    "location" : "View3D",
    "warning" : "",
    "category" : "3D"
}

import bpy

from . proj_image import OT_Operator
from . panel import PT_Panel

classes = (OT_Operator, PT_Panel)

register, unregister = bpy.utils.register_classes_factory(classes)